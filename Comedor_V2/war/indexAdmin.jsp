<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="servlets.*"%>
<%@ page import="java.util.List"%>

<%
	List<Pensionista> pensionistas = (List<Pensionista>) request.getAttribute("pensionistas");
	List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios");
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>Comedor Universitario</title>

<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css" />
<link href="css/nivo-lightbox.css" rel="stylesheet" />
<link href="css/nivo-lightbox-theme/default/default.css"
	rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
<link href="css/owl.theme.css" rel="stylesheet" media="screen" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/animate.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="color/default.css" rel="stylesheet">

<link href="css/main/buscarUsuario.css" rel="stylesheet">
<link href="css/main/miStyle.css" rel="stylesheet"/>



</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">



		



	<!-- Navigation -->
	<div id="navigation">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="site-logo">
							<a href="index.html" class="brand">Bienvenido Administrador</a>
						</div>
					</div>


					<div class="col-md-10">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target="#menu">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="menu">
							<ul class="nav navbar-nav navbar-right">

								<li class="active"><a href="#estadistica">Voto</a></li>
								<li class="active"><a href="cerrarSesion">Salir</a></li>


								<li><a href="#addUser">Agregar Usuario</a></li>
								<li><a href="#lis_pensionista">Pensionista</a></li>
							</ul>
						</div>
						<!-- /.Navbar-collapse -->

					</div>
				</div>
			</div>
			<!-- /.container -->
		</nav>
	</div>
	<!-- /Navigation -->

	<!-- CRUD Usuarios -->


	<section id="usuarios">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Usuarios</h2>
							<div class="divider-header"></div>
							<p>Lista de todos los Alumnos aceptados en el Comedor.</p>
						
						</div>
					</div>
				</div>
			</div>
			<div class="row wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s" >

				
				<div class="form-group pull-right ">
					<input type="text" class="search form-control" placeholder="Busque un Usuario">
				</div>
				<div class="row">
					<span class="col-md-3 col-sm-12 col-xs-12">Subir archivo Excel
					    <button type="submit" class="btn btn-info pull-left" data-input="false"><span class="glyphicon glyphicon-cloud-upload"></span></button> 
					</span>
					<span class="col-md-3 col-sm-12 col-xs-12">Agregar Nuevo Usuario
					   
					    <button type="submit" class="btn btn-info pull-left" id="agregarUsuario"><span class="glyphicon glyphicon-plus-sign"></span></button> 
					   

					</span>
					<span class="col-md-3 col-sm-12	col-xs-12">Borrar Todos los Usuarios
					  <button type="submit" class="btn btn-info pull-left"><span class="glyphicon glyphicon-trash"></span></button> 
					</span>

				</div>
				<!-- tabla usuarios -->
				<span class="counter pull-right"></span>
				<table class="table table-hover table-bordered results" id="tablaUsuario" >
					<thead>
						<tr>
							<th>Nro</th>
						    <th class="col-md-2 col-xs-2">CUI</th>
						    <th class="col-md-2 col-xs-2">DNI</th>
						    <th class="col-md-3 col-xs-3">Apellidos</th>
						    <th class="col-md-3 col-xs-3">Nombre</th>
						    <th class="col-md-2 col-xs-2">Accion</th>

						</tr>
						<tr class="warning no-result">
						      <td colspan="4"><i class="fa fa-warning"></i> No result</td>
						</tr>
					</thead>
					<tbody>
						
							<%
								if (usuarios != null) {
									int i = 1;
									for (Usuario u : usuarios) {
							%>
										<tr>
											<th scope="row"><%=i%></th>
											<td><%=u.getCui()%></td>
											<td><%=u.getDni()%></td>
											<td><%=u.getApellidos()%></td>
											<td><%=u.getName()%></td>
											<td> <button type="submit" class="btn btn-success pull-left"><span class="glyphicon glyphicon-pencil"></span>		</button> 
												<button type="submit" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span>		</button> </td>	
											
										</tr>
							<%
									i++;
									}
								}
							%>


					</tbody>
				</table>
				<!-- /tabla usuarios -->

			</div>
		</div>
		

	</section>

 			<!-- Modal Crear Usuario -->
					<div class="modal fade" id="myModal" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-user"></span> Nuevo Usuario</h4>
						        </div>
						        <div class="modal-body" style="padding:40px 50px;">
							      	<form>
							            <div class="form-group">
								            <label for="cui"><span class="glyphicon glyphicon-tag"></span> CUI</label>
							    	        <input type="number" class="form-control" id="cui" placeholder="Ingrese codigo del alumno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="dni"><span class="glyphicon glyphicon-tag"></span> DNI</label>
							            	<input type="number" class="form-control" id="dni" placeholder="Ingrese Documento de Identidad" required>
							            </div>
							            <div class="form-group">
							        	    <label for="apellidop"><span class="glyphicon glyphicon-tag"></span> Apellido Paterno</label>
							            	<input type="text" class="form-control" id="apaterno" placeholder="Ingrese Apellido Paterno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="apellidom"><span class="glyphicon glyphicon-tag"></span> Apellido Materno</label>
							            	<input type="text" class="form-control" id="amaterno" placeholder="Ingrese Apellido Materno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="nombre"><span class="glyphicon glyphicon-tag"></span> Nombre</label>
							            	<input type="text" class="form-control" id="name" placeholder="Ingrese Nombre" required>
							            </div>
							              <button type="submit" id="registrar" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Agregar</button>
							      	</form>
							    </div>
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal crear Usuario-->





	<!-- /CRUD Usuarios -->


	<!-- Section: about -->
	<!-- /Section: about -->


	<!--  List Pensionista -->

	<section id="lis_pensionista"
		class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container">
			<div class="row marginbot-80">

				<h4>Lista de Pensionistas</h4>

				<div class="col-md-2 col-md-offset-2"></div>
				<div class="col-md-8 col-md-offset-2" id="table_Usuario">

					<table class="table table-hover">
						<thead>
							<tr>
								<th>Nro</th>
								<th>CUI</th>
								<th>Nombre</th>
								<th>Apellidos</th>
								<th>Escuela</th>
								<th>DNI</th>
								<th>E-mail</th>
								<th>Estado</th>
								<th>Select</th>
								<th>Update</th>
							</tr>
						</thead>
						<tbody>
							<%
								if (pensionistas != null) {

									int i = 1;
									for (Pensionista p : pensionistas) {
							%>
							<tr>
								<td><%=i%></td>
								<td><%=p.getCui()%></td>
								<td><%=p.getName()%></td>
								<td><%=p.getApellidos()%></td>
								<td><%=p.getEscuela()%></td>
								<td><%=p.getDni()%></td>
								<td><%=p.getEmail()%></td>
								<td><%=p.getNameEstado()%></td>
								<td><input type="checkbox" name="option[]" id="option[] "
									value='<%=p.getIdPensionista()%>' /></td>
								<td><button type="button" name="_actualizar"
										class="_update" value='<%=p.getIdPensionista()%>' />Update
									</button></td>

							</tr>
							<%
								i++;
									}
								}
							%>

						</tbody>
					</table>

				</div>
				<div class="col-md-2 col-md-offset-2"></div>

			</div>
			
			<div class="row marginbot-80">
				<div class="col-md-2 col-md-offset-2"></div>
				<div class="col-md-8 col-md-offset-2">						
					<button type="button" data-toggle="modal"
						data-target="#modal_eliminar">Eliminar</button>
				</div>
				<div class="col-md-2 col-md-offset-2"></div>
			</div>
			<div class="fila"></div>
		</div>

	</section>
	<!-- /Section: contact -->

<section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">

					<div class="text-center">
						<a href="#estadistica" class="totop"><i
							class="fa fa-angle-up fa-3x"></i></a>

						<p>
							Peru, Avenida Alfonso Ugarte, 121 Jesus Maria Paucarpata,
							Arequipa<br /> &copy;Copyright 2016 - Shuffle. Designed by <a
								href="http://facebook.com/mauricioMaldonado">Mauricio
								Maldonado</a>
						</p>

					</div>
				</div>
			</div>
		</div>
	</footer>
</section>
	<!-- Core JavaScript Files -->



	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

	<script src="js/custom.js"></script>

	<script src="js/main/buscarUsuario.js"></script>
	<script src="js/main/mainAdmin.js"></script>




	<script>
		$(document).ready(function() {
			//registrar usuario
			$('#registrar').click(function(event) {

				event.preventDefault();
				var varCui = $('#cui').val();
				var varDni = $('#dni').val();
				var varPat = $('#apaterno').val();
				var varName = $('#name').val();
				var varMat = $('#amaterno').val();
				//var varEstado = $('#estado').val();
				alert("registrando!");

				
				$.get('registrarUsuario', {
					cui : varCui,
					dni : varDni,
					apaterno : varPat,
					amaterno : varMat,
					name : varName
					

				}, function(responseText) {
					alert(responseText);
					$("#myModal").modal("hide");
					update_users();
				});

			});
			
			function update_users(){
			//	get que llame al servlet mostrar usuario
				$.get('usuarioServlet', {
										

				}, function(responseText) {
					$('#tablaUsuario').html(responseText);
					
				});
			}

			//actualizar
			$('._update').click(function(event) {
				var val = $(this).val();

				$.get('recoveservicio', {
				//clave:val
				}, function(responseText) {
					$('#update_servicio').html(responseText);
				});

			});

			//eliminar
			$('#_eliminar').click(function(event) {

				var varIds = "";

				$("input[name='option[]']:checked").each(function() {
					varIds += ($(this).val()) + " ";
				});
				$.get('deletePensionista', {
					valores : varIds
				}, function(responseText) {

					$('#table_Usuario').html(responseText);
				});

			});

			//agregar
			

		});
	</script>

	<!-- Modal Delete -->

	<div class="modal fade" id="modal_eliminar" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Eliminar</h4>
				</div>
				<div class="modal-body">
					<p>Esta Seguro de Borrar?</p>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal">Cancelar</button>
					<button type="button" data-dismiss="modal" id="_eliminar">Eliminar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- fin delete -->





</body>

</html>