
<%@ page import="clases.*"%>
<%@ page import="servlets.*"%>
<%@ page import="java.util.List"%>



<%
	List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios");
%>

<%-- Tabla que muestra el despues de registrar Usuario --%>

					<thead>
						<tr>
							<th>Nro</th>
						    <th class="col-md-2 col-xs-2">CUI</th>
						    <th class="col-md-2 col-xs-2">DNI</th>
						    <th class="col-md-3 col-xs-3">Apellidos</th>
						    <th class="col-md-3 col-xs-3">Nombre</th>
						    <th class="col-md-2 col-xs-2">Accion</th>

						</tr>
						<tr class="warning no-result">
						      <td colspan="4"><i class="fa fa-warning"></i> No result</td>
						</tr>
					</thead>
					<tbody>


							<%
								if (usuarios != null) {
									int i = 1;
									for (Usuario u : usuarios) {
							%>
										<tr>
											<th scope="row"><%=i%></th>
											<td><%=u.getCui()%></td>
											<td><%=u.getDni()%></td>
											<td><%=u.getApellidos()%></td>
											<td><%=u.getName()%></td>
											<td> <button type="submit" class="btn btn-success pull-left"><span class="glyphicon glyphicon-pencil"></span>		</button> 
												<button type="submit" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-trash"></span>		</button> </td>	
											
										</tr>
							<%
									i++;
									}
								}
							%>
					</tbody>


