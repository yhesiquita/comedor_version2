<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="java.util.List"%>

<%
	List<Nutricionista> nutricionistas = (List<Nutricionista>) request.getAttribute("nutricionistas");
int comida1 = 0, comida2 = 0, comida3 = 0, comida4 = 0;
Nutricionista nab=new Nutricionista("papa rebozada","pastel de papa","camote al horno","caldo blanco");
%>

<%HttpSession misesion= request.getSession(); %>
<%if(misesion.getAttribute("cui") == null){ 			
				response.sendRedirect("index.html");
	}
else {
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pagina de consulta del usuario">
    <meta name="author" content="">

    <title>Comedor Universitario</title>
	
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="css/nivo-lightbox.css" rel="stylesheet" />
	<link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="css/flexslider.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
	<link href="color/default.css" rel="stylesheet">
	
	
	    <!-- Core JavaScript Files -->
    <script src="js/jquery.min.js"></script>	 
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.easing.min.js"></script>	
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

    <script src="js/custom.js"></script>
    
    
	<style>
	table {
    border: 1px solid black;
    border-collapse: collapse;
}
	th, td {
    padding: 5px;
    text-align:center;
}
</style>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	
	<section id="intro" class="home-slide text-light">

		<!-- Carousel -->
    	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
			  	<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
			    <div class="item active">
			    	<img src="img/1.jpg" alt="First slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                            	<span>Bienvenido Pensionista Online</span>
                            </h2>
                            <br>
                            <h3>
                            	<span> >>>Donde Podras elejir QUE COMER en nuestro comedor.</span>
                            </h3>
                            <br>
                            <div class="">
                                 <a class="btn btn-theme btn-sm btn-min-block" href="#registrar">Registrate</a><a class="btn btn-theme btn-sm btn-min-block" href="#loggear">Loggeate</a></div>
                        </div>
                    </div><!-- /header-text -->
			    </div>
			    <div class="item">
			    	<img src="img/2.jpg" alt="Second slide">
			    	<!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                <span>1</span>
                            </h2>
                            <br>
                            <h3>
                            	<span>Registrate para que puedas acceder a nuestros servicios.</span>
                            </h3>
                            <br>
                            <div class="">
                                 <a class="btn btn-theme btn-sm btn-min-block" href="#registrar">Registrate</a><a class="btn btn-theme btn-sm btn-min-block" href="#loggear">Loggeate</a></div>
                        </div>
                    </div><!-- /header-text -->
			    </div>
			    <div class="item">
			    	<img src="img/3.jpg" alt="Third slide">
			    	<!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                <span>2</span>
                            </h2>
                            <br>
                            <h3>
                            	<span>Vota por el Menu del dia que mas te guste.</span>
                            </h3>
                            <br>
                            <div class="">
                                <a class="btn btn-theme btn-sm btn-min-block" href="#registrar">Registrate</a><a class="btn btn-theme btn-sm btn-min-block" href="#loggear">Loggeate</a></div>
                        </div>
                    </div><!-- /header-text -->
			    </div>
			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div><!-- /carousel -->

	</section>
	<!-- /Section: intro -->
	
	
    <!-- Navigation -->
    <div id="navigation">
        <nav class="navbar navbar-custom" role="navigation">
                              <div class="container">
                                    <div class="row">
                                          <div class="col-md-2">
                                                   <div class="site-logo">
                                                            <a href="index.html" class="brand">Bienvenido "Usuario"</a>
                                                    </div>
                                          </div>
                                          

                                          <div class="col-md-10">
                         
                                                      <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
                                          </div>
                                                      <!-- Collect the nav links, forms, and other content for toggling -->
                                                      <div class="collapse navbar-collapse" id="menu">
                                                            <ul class="nav navbar-nav navbar-right">
                                                                  <li class="active"><a href="#intro">Home</a></li>
                                                                  <li><a href="#about">Tabla estadistica</a></li>
																  <li><a href="#service">Services</a></li>
                                                                  <li><a href="#works">Votacion</a></li>				                                                                  
                                                                  
                                                                  <li><a href="#contact">Contact</a></li>
                                                                  <li class="active"><a href="cerrarSesion">Salir</a></li>
                                                                  <li><a href="#cambiar">Cambiar contraseņa</a></li>
                                                            </ul>
                                                      </div>
                                                      <!-- /.Navbar-collapse -->
                             
                                          </div>
                                    </div>
                              </div>
                              <!-- /.container -->
                        </nav>
    </div> 
    <!-- /Navigation -->  

	<!-- Section: about -->
    <section id="about" class="home-section color-dark bg-white">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Votos por Dia</h2>
					<div class="divider-header"></div>
					<p>Las comidas mas votadas hoy 07 de julio 2016.</p>
					</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

		
        <div class="row">
		
			<div class="col-md-6">
			    <img src="img/dummy1.jpg" alt="" class="img-responsive" />
      
           </div>
			<%
		     
		     	
				if (session.getAttribute("1") != null){
					comida1 = (Integer) session.getAttribute("1");
					}
				if (session.getAttribute("2") != null){
					comida2 = (Integer) session.getAttribute("2");
					}
				if (session.getAttribute("3") != null) {
					comida3 = (Integer) session.getAttribute("3");
					}
				if (session.getAttribute("4") != null){
					comida4 = (Integer) session.getAttribute("4");
					}
 			String c1=(Integer) session.getAttribute("pp1")+"%";
 			String c2=(Integer) session.getAttribute("pp2")+"%";
			String c3=(Integer) session.getAttribute("pp3")+"%";
 			String c4=(Integer) session.getAttribute("pp4")+"%";
			%>
            <div class="col-md-6">		
			<p>Porcentaje de las comidas de hoy.</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c1%>"><%=nab.getComida1()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c2%>"><%=nab.getComida2()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" 
			  aria-valuemin="0" aria-valuemax="100" style="width:<%=c3%>"><%=nab.getComida3()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c4%>"><%=nab.getComida4()%>
			  </div>
			</div>

            </div>
		

        </div>		
		</div>

	</section>
	<!-- /Section: about -->


			
		    	<!-- Section: works -->
							<script>
					
					
// 							Cre una base de da tos solo para guardar el numero de vvotosque deseo sino no funcionara		
					function myFunction5() {
				
						alert("Votos emitidos:\n"
								+"\n<%=nab.getComida1()%>:" +"<%=comida1%>"
								+"\n<%=nab.getComida2()%>: "+"<%=comida2%>"
								+"\n<%=nab.getComida3()%>: "+"<%=comida3%>"
								+"\n<%=nab.getComida4()%>: "+"<%=comida4%>"
								+"\n<%=c1%>"+"\n<%=c2%>"+"\n<%=c3%>"+"\n<%=c4%>");
						
						
					}
	

					</script>


									
    <section id="works" class="home-section color-dark text-center bg-white">
<!-- esta parte --------------------------------------------- -->
	
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Comidas para Lunes 30 de Mayo 2016</h2>
					
					<div class="divider-header"></div>
					
					
					<p>Vote por la comida que mas le agrade.</p>
					<p id="demo"></p>
 					<form action="nutricionistaServlet" method="post">
 					<select name="candidato">
 					<option value="1"><%=nab.getComida1()%></option>
 					<option value="2"><%=nab.getComida2()%></option>
 					<option value="3"><%=nab.getComida3()%></option>
 					<option value="4"><%=nab.getComida4()%></option>
 					</select>
 					<br><br>
 					<button  type="submit" name="enviar">Votar</button>
 					 					</form> 

						<button onclick="myFunction5()">Ver</button>
							
					</div>
					</div>
				</div>
			</div>

		</div>
					

		<div class="container">
			<div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" >
					<div class="wow bounceInUp" data-wow-delay="0.4s">
                    <div id="owl-works" class="owl-carousel">
                        <div class="item"><a href="img/works/1.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg"><img src="img/works/1.jpg" class="img-responsive" alt="img"></a></div>
                        <div class="item"><a href="img/works/2.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/2@2x.jpg"><img src="img/works/2.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/3.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/3@2x.jpg"><img src="img/works/3.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/4.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/4@2x.jpg"><img src="img/works/4.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/5.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/5@2x.jpg"><img src="img/works/5.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/6.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/6@2x.jpg"><img src="img/works/6.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/7.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/7@2x.jpg"><img src="img/works/7.jpg" class="img-responsive " alt="img"></a></div>
                        <div class="item"><a href="img/works/8.jpg" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/8@2x.jpg"><img src="img/works/8.jpg" class="img-responsive " alt="img"></a></div>
                        
                    </div>
					</div>
                </div>
            </div>
		</div>

	</section>
	<!-- /Section: works -->

	<!-- Section: contact -->
    <section id="contact" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Contact us</h2>
					<div class="divider-header"></div>
					<p>Lorem ipsum dolor sit amet, agam perfecto sensibus usu at duo ut iriure.</p>
					</div>
					</div>
				</div>
			</div>

		</div>
		
		<div class="container">

			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
						<form id="contact-form" class="wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s">
						<div class="row marginbot-20">
							<div class="col-md-6 xs-marginbot-20">
								<input type="text" class="form-control input-lg" id="name" placeholder="Enter name" required="required" />
							</div>
							<div class="col-md-6">
								<input type="email" class="form-control input-lg" id="email" placeholder="Enter email" required="required" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
										<input type="text" class="form-control input-lg" id="subject" placeholder="Subject" required="required" />
								</div>
								<div class="form-group">
									<textarea name="message" id="message" class="form-control" rows="4" cols="25" required="required"
										placeholder="Message"></textarea>
								</div>						
								<button type="submit" class="btn btn-skin btn-lg btn-block" id="btnContactUs">
									Send Message</button>
							</div>
						</div>
						</form>
				</div>
			</div>	


		</div>
	</section>
	<!-- /Section: contact -->


	<!-- Section: cambiar contraseņa -->
    <section id="cambiar" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Cambiar contraseņa</h2>
					<div class="divider-header"></div>
					<p></p>
					</div>
					</div>
				</div>
			</div>

		</div>
		
		<div class="container">

			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
						
						<form id="contact-form" class="wow bounceInUp" action="cambiarContra" method="post" data-wow-offset="10" data-wow-delay="0.2s">
						
						  
						
						<div class="row">
							<div class="col-md-12">
								<!--  
								<div class="form-group">
										<input type="text" class="form-control input-lg" name="cui3" id="cui3" placeholder="CUI" required="required" />
								</div>
								
								-->
								<div class="form-group">
										<input type="password" class="form-control input-lg" name="con1" id="con1" placeholder="contraseņa" required="required" />
								</div>
								<div class="form-group">
										<input type="password" class="form-control input-lg" name="con2" id="con2" placeholder="nueva contraseņa" required="required" />
								</div>						
								<button type="submit" class="btn btn-skin btn-lg btn-block" id="cambiar">
									Modificar Contraseņa</button>
							</div>
						</div>
						</form>
				</div>
			</div>	


		</div>
	</section>
	<!-- /Section: cambiar comtraseņa -->



	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					
					<div class="text-center">
						<a href="#intro" class="totop"><i class="fa fa-angle-up fa-3x"></i></a>

						<p>Peru, Avenida Alfonso Ugarte, 121 Jesus Maria Paucarpata, Arequipa<br/>
						&copy;Copyright 2016 - Shuffle. Designed by <a href="http://facebook.com/mauricioMaldonado">Mauricio Maldonado</a></p>
                                              
					</div>
				</div>
			</div>	
		</div>
	</footer>



</body>

</html>
<%} %>