package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import clases.PMF;
import clases.Pensionista;

@SuppressWarnings("serial")
public class IniciarSesion extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//resp.setContentType("text/plain");

		resp.setContentType("text/html");
		int registrado =0;//1: admin ,2: nutricionista, 3: pensionista y 0:nadie

		if( req.getParameter("cui")!=null  && req.getParameter("pass")!=null){

			String cui = req.getParameter("cui");
			String pass = req.getParameter("pass");

			if(cui.equals("admin") && pass.equals("1234")){
				registrado = 1;
			}
			else if(cui.equals("nutri") && pass.equals("1234")){
				registrado = 2;
			}
			else{
				final PersistenceManager pm = PMF.get().getPersistenceManager();
				final Query q = pm.newQuery(Pensionista.class);
				q.setOrdering("idPensionista ascending");
				q.setFilter("cui == cuiParam && password == passParam");
				q.declareParameters("String cuiParam, String passParam");

				try{
					@SuppressWarnings("unchecked")
					List<Pensionista> pensionistas = (List<Pensionista>) q.execute(cui,pass);
					PrintWriter out = resp.getWriter();
					if(pensionistas.size()!=0 ){
						registrado = 3;
					}
					out.close();

				}catch(Exception e){
					System.out.println(e);
				}finally{
					q.closeAll();
					pm.close();
				}
			}
			if(registrado!=0){
				HttpSession misesion= req.getSession();
				misesion.setAttribute("cui",cui);
				misesion.setAttribute("pass",pass);				
				String a = (String) misesion.getAttribute("cui");
				String b = (String) misesion.getAttribute("pass");				
				System.out.println(a);
				System.out.println(b);
				Date date = new Date();
				misesion.setAttribute("inicio", date.toString());
				if(registrado == 1){//admin

					resp.sendRedirect("pensionistaServlet");
				}
				else if(registrado == 2){//nutricionista

					resp.sendRedirect("/indexNutricionista.html");
				}else {//pensionista
					resp.sendRedirect("/indexUsuario.jsp");
				}
			}
			else{
				resp.sendRedirect("/index.html");
			}
		}
		else{
			resp.getWriter().println("nulll");
		}

	}
}

