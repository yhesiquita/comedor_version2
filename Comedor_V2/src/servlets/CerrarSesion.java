package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class CerrarSesion extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		HttpSession misesion= req.getSession();
		misesion.invalidate(); //Cierra la sesion
		try{
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.html");
			rd.forward(req, resp);
		}catch(Exception e){
			System.out.println(e);
		}

	}
}