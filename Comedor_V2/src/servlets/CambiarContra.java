package servlets;
import java.io.IOException;

import clases.*;
import java.io.PrintWriter;
import java.util.List; 
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
public class CambiarContra extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		/*
		//PersistenceManager pm = PMF.get().getPersistenceManager();
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		//Query q = pm.newQuery(Pensionista.class);
		
		//HttpSession sesion = req.getSession();
	    //String cui = (String)sesion.getAttribute("cui");		
	    String cui = req.getParameter("cui3");
	    String pass = req.getParameter("con1");
		String con2 = req.getParameter("con2");
		
		System.out.println(cui);
		System.out.println(pass);
		System.out.println(con2);
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Pensionista.class);
		q.setOrdering("idPensionista ascending");
		q.setFilter("cui == cuiParam");
		q.declareParameters("String cuiParam");

		try{
			@SuppressWarnings("unchecked")
			List<Pensionista> pensionistas = (List<Pensionista>) q.execute(cui);			
			for (Pensionista c : pensionistas) {
				if(c.getPassword().equals(pass)){
					System.out.println("c1" +c);
					c.setPassword(con2);
					System.out.println("c2"+ c);
					//out.print("Contraseņa Modificada");
					out.print("<script>javascript:alert('Contraseņa Modificada')</script>");
					break;
				}else{
					//out.print("Contraseņa incorrecta");
					out.print("<script>javascript:alert('Contraseņa incorrecta')</script>");
					
				}
			}
		}catch(Exception e){
			System.out.println(e);
			out.print("Error");
		}finally{
			out.close();
			q.closeAll();
			pm.close();
		}
		
		*/
		
		
		//PersistenceManager pm = PMF.get().getPersistenceManager();
				PrintWriter out = resp.getWriter();
				resp.setContentType("text/html");
				//Query q = pm.newQuery(Pensionista.class);			
				HttpSession misesion = req.getSession();
			    String cui = (String)misesion.getAttribute("cui");
			    
			    String pass = req.getParameter("con1");
			    
				String con2 = req.getParameter("con2");
				
				System.out.println("MIRA MI CUI :"+cui);
				System.out.println(pass);
				System.out.println(con2);
				
				final PersistenceManager pm = PMF.get().getPersistenceManager();
				final Query q = pm.newQuery(Pensionista.class);
				q.setOrdering("idPensionista ascending");
				q.setFilter("cui == cuiParam");
				q.declareParameters("String cuiParam");

				try{
					@SuppressWarnings("unchecked")
					List<Pensionista> pensionistas = (List<Pensionista>) q.execute(cui);			
					for (Pensionista c : pensionistas) {
						if(c.getPassword().equals(pass)){
							System.out.println("c1" +c);
							c.setPassword(con2);
							System.out.println("c2"+ c);
							out.print("Contraseņa Modificada");
							//out.print("<script>javascript:alert('Contraseņa Modificada')</script>");
							break;
						}else{
							out.print("Contraseņa incorrecta");
							//out.print("<script>javascript:alert('Contraseņa incorrecta')</script>");
							
						}
					}
				}catch(Exception e){
					System.out.println(e);
					out.print("Error");
				}finally{
					out.close();
					q.closeAll();
					pm.close();
				}
	}
}