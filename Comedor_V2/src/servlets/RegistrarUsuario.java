package servlets;

import clases.*;

import java.io.IOException;
//import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@SuppressWarnings("serial")
public class RegistrarUsuario extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
			resp.setContentType("text/html");
		
				
			String cui = req.getParameter("cui");
			String name = req.getParameter("name");
			String apaterno = req.getParameter("apaterno");
			String amaterno = req.getParameter("amaterno");
			String dni = req.getParameter("dni");			
			
			
			Usuario user = new Usuario(cui,name,apaterno, amaterno,dni,1);
			
			PersistenceManager pm = PMF.get().getPersistenceManager();
			try{
				pm.makePersistent(user);
				
				resp.getWriter().println("saving ...");			
		
			}catch(Exception e){
				System.out.println(e);
				resp.getWriter().println("Ocurrio un error, <a href='index.html'>vuelva a intentarlo</a>");
			}finally{
				pm.close();
			}
		
		
	}
}
