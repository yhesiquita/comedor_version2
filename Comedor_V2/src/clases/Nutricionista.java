
package clases;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Nutricionista {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idPensionista;	

	@Persistent
	private String comida1;
	@Persistent
	private String comida2;
	@Persistent
	private String comida3;
	@Persistent
	private String comida4;
	
	@Persistent 
	private int n1;
	@Persistent
	private int n2;
	@Persistent
	private int n3;
	@Persistent
	private int n4;
	
	public Nutricionista(){
		comida1="Nada";
		comida2="Nada";
		comida3="Nada";
		comida4="Nada";
		n1=0;
		n2=0;
		n3=0;
		n4=0;
	}
	public Nutricionista(int n1, int n2, int n3,
			int n4 ) {

		this.n1=n1;
		this.n2=n2;
		this.n3=n3;
		this.n4=n4;
	}
	
	


	public Nutricionista(String comida1, String comida2, String comida3,
			String comida4 ) {
		
		this.comida1 = comida1;
		this.comida2 = comida2;
		this.comida3 = comida3;
		this.comida4 = comida4;
		this.n1=0;
		this.n2=0;
		this.n3=0;
		this.n4=0;
	}
	

	

	public String getComida1() {
		return comida1;
	}
	
	public String getComida2() {
		return comida2;
	}
	
	public String getComida3(){
		return comida3;
		}
		
	
	
	public String getComida4() {
		return comida4;
	}

	public void setComida1(String comida1) {
		this.comida1 = comida1;
	}
	public void setComida2(String comida2) {
		this.comida2 = comida2;
	}
	public void setComida3(String comida3) {
		this.comida3 = comida3;
	}
	public void setComida4(String comida4) {
		this.comida4 = comida4;
	}


	public int getN1() {
		return n1;
	}



	public void setN1(int n1) {
		this.n1=n1;
	}



	public int getN2() {
		return n2;
	}



	public void setN2(int n2) {
		this.n2 = n2;
	}



	public int getN3() {
		return n3;
	}



	public void setN3(int n3) {
		this.n3 = n3;
	}



	public int getN4() {
		return n4;
	}



	public void setN4(int n4) {
		this.n4 = n4;
	}

	
	
}
